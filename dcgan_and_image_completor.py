import numpy as np
from zipfile import ZipFile
from keras.optimizers import *
import matplotlib.pyplot as plt
from google.colab import drive  # for using colab
from dcgan_utils import *

drive.mount('/content/drive')
file_name = "/content/drive/My Drive/faces_data/Archive.zip"


with ZipFile(file_name, 'r') as _zip:
    data = _zip.read('.npy')


class DCGAN():
    def __init__(self, data_set, z_shape=100, lamb=0.3):
        self.data = data_set
        self.X_train = None
        self.X_test = None
        self.z_shape = z_shape
        self.generator = None
        self.discriminator = None
        self.gen_optimizer = Adam(lr=0.00015, beta_1=0.5)
        self.disc_optimizer = Adam(lr=0.00015, beta_1=0.5)
        self.combined = None
        self.img_completor = None
        self.lamb = lamb
        self.z = None

    @staticmethod
    def scale_images(images, inv=True):
        if not inv:
            images = images * 2 - 1
        else:
            images = images * 0.5 + 0.5
        return images

    def load_data(self, num_of_train_imgs=30000):
        self.X_train = self.data
        self.X_train = np.frombuffer(self.X_train, dtype='float32')
        self.X_train = self.X_train[32:]
        self.X_train = self.X_train.reshape(70000, 64, 64, 3)
        self.X_train = self.scale_images(self.X_train, inv=False)
        self.X_test = self.X_train[num_of_train_imgs:]
        self.X_train = self.X_train[:num_of_train_imgs]

    def init_discriminator(self, img_shape):
        self.discriminator = build_discriminator(img_shape)

    def init_generator(self):
        self.generator = build_generator(self.z_shape)

    def init_model(self):
        self.init_discriminator(img_shape=(64, 64, 3))
        self.init_generator()
        self.z = Input(shape=(self.z_shape,))
        img = self.generator(self.z)
        self.discriminator.compile(loss='binary_crossentropy',
                                   optimizer=self.disc_optimizer,
                                   metrics=['accuracy'])
        self.discriminator.trainable = False
        real_or_fake = self.discriminator(img)
        self.combined = Model(self.z, real_or_fake)
        self.generator.compile(loss='binary_crossentropy', optimizer=self.gen_optimizer)
        self.combined.compile(loss='binary_crossentropy', optimizer=self.gen_optimizer)

    def _load_model(self):
        self.generator = models.load_model('/content/drive/My Drive/DL_Project/generator2.HDF5')
        self.discriminator = models.load_model('/content/drive/My Drive/DL_Project/discriminator2.HDF5')
        self.combined = models.load_model('/content/drive/My Drive/DL_Project/combined2.HDF5')

    def _save_model(self):
        self.combined.save('/content/drive/My Drive/faces_data/combined3.HDF5')
        self.generator.save('/content/drive/My Drive/faces_data/generator3.HDF5')
        self.discriminator.save('/content/drive/My Drive/faces_data/discriminator3.HDF5')

    def print_images(self, epoch=None):
        random_noise = np.random.normal(0, 1, (5, self.z_shape))
        gen_imgs = self.generator.predict(random_noise)
        gen_imgs = self.scale_images(gen_imgs)
        plt.figure(figsize=(10, 5))
        plt.subplot(151)
        plt.imshow(gen_imgs[0])
        plt.axis("off")
        plt.subplot(152)
        plt.imshow(gen_imgs[1])
        plt.axis("off")
        plt.subplot(153)
        plt.imshow(gen_imgs[2])
        if epoch == 1:
            plt.title("After -1 epoch")
        else:
            plt.title(f"After -{epoch} epochs")
        plt.axis("off")
        plt.subplot(154)
        plt.imshow(gen_imgs[3])
        plt.axis("off")
        plt.subplot(155)
        plt.imshow(gen_imgs[4])
        plt.axis("off")
        plt.show()

    def train(self, batch_size=32, epochs=20):
        num_examples = self.X_train.shape[0]
        num_batches = int(num_examples / float(batch_size))
        half_batch = int(batch_size / 2)

        for epoch in range(epochs):
            for batch in range(num_batches):
                # noise images for the batch
                random_noise = np.random.normal(0, 1, (half_batch, self.z_shape))
                fake_images = self.generator.predict(random_noise)
                fake_labels = np.zeros((half_batch, 1))

                # real images for batch
                idx = np.random.randint(0, self.X_train.shape[0], half_batch)
                real_images = self.X_train[idx]
                real_labels = np.ones((half_batch, 1))

                # Train the discriminator (real classified as ones and generated as zeros)
                d_loss_real = self.discriminator.train_on_batch(real_images, real_labels)
                d_loss_fake = self.discriminator.train_on_batch(fake_images, fake_labels)

                d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
                random_noise = np.random.normal(0, 1, (batch_size, self.z_shape))
                # Train the generator
                g_loss = self.combined.train_on_batch(random_noise, np.ones((batch_size, 1)))
                # Plot the progress
                print("Epoch %d Batch %d/%d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (
                    epoch, batch, num_batches, d_loss[0], 100 * d_loss[1], g_loss))


class ImageCompletor:
    def __init__(self, dcgan):
        self.z = np.load('avg_z.npy')
        self.real_img = None
        self.fake_img = None
        self.mask_coords = None
        self.mask = None
        self.corr_img = None
        self.result = None
        self.lamb = None
        self.save_fig = False
        self.dcgan = dcgan

    def loss(self, gen_img):
        c_loss = abs(gen_img * self.mask - self.corr_img).sum()
        p_loss = np.log(1 - self.dcgan.discriminator.predict(gen_img))
        curr_loss = c_loss + self.lamb * p_loss
        return curr_loss[0][0]

    def corrupt_img(self):
        """init self.mask according to 'mask_coords' and applies it on 'real_img' using hadamard product"""
        self.mask = np.ones_like(self.real_img)
        self.mask[self.mask_coords[0]:self.mask_coords[1], self.mask_coords[2]:self.mask_coords[3], :] = 0
        self.corr_img = self.real_img * self.mask

    def minimize(self, lr, iterations):
        for i in range(1, iterations + 1):

            if i % 2000 == 0:
                lr = lr / 2
            self.fake_img = self.dcgan.scale_images(self.dcgan.generator.predict(self.z))
            z_loss = self.loss(self.fake_img)
            print(f'iteration {i}/{iterations} ............. loss = {round(z_loss, 2)}')
            d_z = np.random.normal(0, 1, (1, 100))
            d_z = d_z / np.linalg.norm(d_z)
            update1 = self.z - lr * d_z
            update2 = self.z + lr * d_z
            # choosing descent direction - either update1 or its opposite - update2
            fake_img_update1 = self.dcgan.scale_images(self.dcgan.generator.predict(update1))
            if z_loss > self.loss(fake_img_update1):
                self.z = update1
            else:
                self.z = update2

    def display_results(self):
        plt.figure(figsize=(20, 10))
        plt.subplot(141)
        plt.axis('off')
        plt.title("Original image", fontsize=20)
        plt.imshow(self.real_img)
        plt.subplot(142)
        plt.axis('off')
        plt.title("Original image \nwith missing block", fontsize=20)
        plt.imshow(self.corr_img)
        plt.subplot(143)
        plt.axis('off')
        plt.title("Completed image", fontsize=20)
        plt.imshow(self.result)
        plt.show()

    def complete(self, mask_coords=(10, 40, 10, 40), iterations=1000, lr=0.1, input_img=None,
                 use_prev_img=False, lamb=0.2):
        self.lamb = lamb  # weight of the perceptual loss
        self.mask_coords = mask_coords

        if input_img is not None:
            self.real_img = input_img
            self.real_img = self.dcgan.scale_images(self.real_img)

        elif use_prev_img:
            assert self.real_img, "No previous image.."

        else:  # select random image from test data
            self.real_img = self.dcgan.X_test[np.random.randint(0, self.dcgan.X_test.shape[0])]
            self.real_img = self.dcgan.scale_images(self.real_img)

        self.corrupt_img()
        self.minimize(lr, iterations)

        self.fake_img = self.dcgan.scale_images(self.dcgan.generator.predict(self.z)[0])
        patch = ((1 - self.mask) * self.fake_img)
        self.result = (self.corr_img + patch)
        self.display_results()
