from keras import *
from keras.layers import *


def build_discriminator(img_shape):
    inp = Input(img_shape)
    x = Conv2D(32, kernel_size=3, strides=2, padding="same")(inp)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dropout(0.25)(x)
    x = Conv2D(64, kernel_size=3, strides=2, padding="same")(x)
    x = ZeroPadding2D(padding=((0, 1), (0, 1)))(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = (LeakyReLU(alpha=0.2))(x)

    x = Dropout(0.25)(x)
    x = Conv2D(128, kernel_size=3, strides=2, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dropout(0.25)(x)
    x = Conv2D(256, kernel_size=3, strides=1, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dropout(0.25)(x)
    x = Conv2D(512, kernel_size=3, strides=1, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dropout(0.25)(x)
    x = Flatten()(x)
    out = Dense(1, activation='sigmoid')(x)
    model = Model(inp, out)
    print("-- Discriminator -- ")
    model.summary()
    return model


def build_generator(input_shape):
    noise_shape = (input_shape,)
    inp = Input(noise_shape)
    x = Dense(4 * 4 * 256, activation="relu")(inp)
    x = Reshape((4, 4, 256))(x)

    x = UpSampling2D()(x)
    x = Conv2D(256, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = Activation("relu")(x)

    x = UpSampling2D()(x)
    x = Conv2D(256, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = Activation("relu")(x)

    x = UpSampling2D()(x)
    x = Conv2D(128, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = Activation("relu")(x)

    x = UpSampling2D()(x)
    x = Conv2D(128, kernel_size=3, padding="same")(x)
    x = BatchNormalization(momentum=0.8)(x)
    x = Activation("relu")(x)

    x = Conv2D(3, kernel_size=3, padding="same")(x)
    out = Activation("tanh")(x)
    model = Model(inp, out)

    print("-- Generator -- ")
    model.summary()
    return model
